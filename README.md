WorkerQueue
===========
Declaration: All the code given here, could be re-used for any educational / learning needs.

Requirement:
When there is a need for optimizing the use of thread pool based on the work it does, this framework could be used
as such in those places. This framework helps is fine tuning the threadpool max count, for optimized usage of CPU
resources based on
1. the hardware the application is run on.
2. the type of function your thread is calling.

PreRequisite:
1. Functor written for thread processing.

Output:
1. The optimized number of threads and queues that could be used for the functor given.
2. Creates the config which could be right away used with the framework.

