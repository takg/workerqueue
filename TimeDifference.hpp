#ifndef TIMEDIFFERENCE_H_INCLUDED
#define TIMEDIFFERENCE_H_INCLUDED

#include <boost/date_time/posix_time/posix_time.hpp>

namespace takg
{
    class TimeDifference
    {
        public:
            TimeDifference()
            {
                reset();
            }
            TimeDifference(const TimeDifference& right):
                start(right.start)
            {
            }
            ~TimeDifference()
            {
                stop();
            }
            TimeDifference& operator=(const TimeDifference& right)
            {
                if (&right != this)
                {
                    start = right.start;
                    end = right.end;
                }
                return *this;
            }
            double elapsed()
            {
                end = boost::posix_time::microsec_clock::local_time();
                time = end - start;
                return time.total_milliseconds();
            }
            void reset()
            {
                start = boost::posix_time::microsec_clock::local_time();
            }
            void stop()
            {
                end = boost::posix_time::microsec_clock::local_time();
                time = (end - start);
            }
            boost::posix_time::time_duration getTime()
            {
                stop();
                return time;
            }
        private:

            boost::posix_time::ptime start;
            boost::posix_time::ptime end;
            boost::posix_time::time_duration time;
    };
}

#endif // TIMEDIFFERENCE_H_INCLUDED
