#ifndef PRINTSTREAM_HPP_INCLUDED
#define PRINTSTREAM_HPP_INCLUDED

#include <iostream>
#include <boost/thread.hpp>

namespace takg
{
    class PrintStream: public std::ostream
    {
    public:
        PrintStream(std::ostream& in_stream)
            :stream(in_stream)
        {
        }
        ~PrintStream()
        {
        }
        PrintStream(const PrintStream& right)
            :stream(right.stream)
        {
        }
        PrintStream& operator=(const PrintStream& right)
        {
            if (this != &right)
            {
                ;
            }
            return *this;
        }

        template <typename T>
        PrintStream& operator<<(T input)
        {
            //boost::mutex::scoped_lock lock(mutex);
            this->stream << input;
            return *this;
        }

    private:
        std::ostream& stream;
        boost::mutex mutex;
    };

    static PrintStream cout(std::cout);
}
#endif // PRINTSTREAM_HPP_INCLUDED
