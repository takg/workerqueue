#include "Simulator.hpp"
#include "PrintStream.hpp"

int main()
{
    takg::cout << "Running the simulator..." << "\n";
    Simulator simulator;
    simulator.run();
    return 0;
}
