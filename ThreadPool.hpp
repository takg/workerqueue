#ifndef THREADPOOL_HPP_INCLUDED
#define THREADPOOL_HPP_INCLUDED

#include <boost/thread.hpp>
#include "PrintStream.hpp"

namespace takg
{
    template <typename Thread=boost::thread, typename Mutex=boost::mutex >
    class ThreadPool
    {
    public:
        ThreadPool(int size, bool lazy)
            :minSize(1),
             maxSize(size),
             poolSize(0)
        {
        }
        ThreadPool(const ThreadPool& right)
            :minSize(right.minSize),
             maxSize(right.maxSize),
             poolSize(right.poolSize)
        {
        }
        ~ThreadPool()
        {
        }
        void join()
        {
            // wait for all the threads to complete
            group.join_all();
        }
        template <typename Functor>
        void start(Functor func)
        {
            boost::mutex::scoped_lock lock(mutex);

            if (poolSize > minSize && poolSize < maxSize)
            {
                // wait for some time and create the thread
                //boost::this_thread::sleep(boost::posix_time::milliseconds(10));
            }
            while(poolSize >= maxSize)
            {
                // keep waiting till one of the threads hasn't completed it's task
                boost::this_thread::sleep(boost::posix_time::milliseconds(100));
            }

            group.create_thread(func);
            ++poolSize;
        }
        ThreadPool& operator=(const ThreadPool& right)
        {
            if (&right != this)
            {
                minSize = right.minSize;
                maxSize = right.maxSize;
                poolSize = 0;
            }
            return *this;
        }
        int currentSizeOfPool()
        {
            return poolSize;
        }
        int maxSizeOfPool()
        {
            return maxSize;
        }
    private:
        int minSize;
        int maxSize;
        int poolSize;
        boost::thread_group group;
        Mutex mutex;
    };
}

#endif // THREADPOOL_HPP_INCLUDED
