#ifndef SIMULATOR_H
#define SIMULATOR_H

#include "ThreadPool.hpp"
#include "QueueManager.hpp"
#include "TimeDifference.hpp"
#include <string>

class Simulator
{
    public:
        Simulator();
        ~Simulator();
        void run();
        void dumpResults();
    protected:
    private:
        static const int THREAD_MAX_POOL = 30;
        static const int QUEUE_MAX_POOL = 30;
        static const long MAX_MESSAGES = 54321;
};

struct PushDataSimulator
{
    PushDataSimulator(long max, takg::QueueManager<std::string>& io_Queue):
        maxLimit(max),
        queue(io_Queue)
    {
    }
    PushDataSimulator(const PushDataSimulator& right):
        maxLimit(right.maxLimit),
        queue(right.queue)
    {
    }
    void operator()()
    {
        for (long nLoop = 0 ; nLoop < maxLimit ; ++nLoop)
        {
            std::stringstream ss;
            ss << nLoop;
            queue.push_back(ss.str());
        }
        return;
    }
private:
    long maxLimit;
    takg::QueueManager<std::string>& queue;
};

struct PullDataSimulator
{
    PullDataSimulator(long max, takg::QueueManager<std::string>& io_Queue):
        count(0),
        maxLimit(max),
        id(-1),
        queue(io_Queue)
    {
    }
    PullDataSimulator(const PullDataSimulator& right):
        count(0),
        maxLimit(right.maxLimit),
        id(right.id),
        queue(right.queue)
    {
    }
    void setID(int inputID)
    {
        id = inputID;
        //takg::cout << "Setting threadID:" << id << "\n";
    }
    void operator()()
    {
        int failCount = 0;
        while(true)
        {
            std::string str;
            if (id != -1)
            {
                if (queue.pop_front(str, id))
                {
                    std::string reverse;
                    for (std::string::reverse_iterator it = str.rbegin() ; it != str.rend(); ++it)
                    {
                        reverse.append(1,*it);
                    }
                    for (int i = 0 ; i < reverse.size() ; ++i)
                    {
                        if (reverse[i])
                        {
                            int weight = 0;
                            weight += reverse[i];
                        }
                    }
                    //takg::cout << reverse << "\n";
                    failCount = 0;
                    ++count;
                }
                else
                {
                    ++failCount;
                    //boost::this_thread::sleep(boost::posix_time::milliseconds(10));
                    if (failCount >= 10)
                    {
                        //takg::cout << "Thread " << id << ": Processed messages = " << count << " Closing..." << "\n";
                        break;
                    }
                }
            }
            else
            {
                if (queue.pop_front(str))
                {
                    // customized logic
                    std::string reverse;
                    for (std::string::reverse_iterator it = str.rbegin() ; it != str.rend(); ++it)
                    {
                        reverse.append(1,*it);
                    }
                    //takg::cout << reverse << " " << str << "\n";
                    failCount = 0;
                    ++count;
                }
                else
                {
                    ++failCount;
                    boost::this_thread::sleep(boost::posix_time::milliseconds(10));
                    if (failCount >= 10)
                    {
                        //takg::cout << "Thread " << id << ": Processed messages = " << count << " Closing..." << "\n";
                        break;
                    }
                }
            }
        }
        return;
    }
private:
    long count;
    long maxLimit;
    int id;
    takg::QueueManager<std::string>& queue;
};

struct SimulatorResults
{
    int threadPool;
    int queuePool;
    long messageCount;
    boost::posix_time::time_duration time;

    SimulatorResults(int threadPoolCount, int queuePoolCount, long maxMessageCount):
        threadPool(threadPoolCount),
        queuePool(queuePoolCount),
        messageCount(maxMessageCount)
    {
    }
    bool operator==(const SimulatorResults& right) const
    {
        return (threadPool == right.threadPool && queuePool == right.queuePool && messageCount == right.messageCount);
    }
    bool operator!=(const SimulatorResults& right) const
    {
        return !(operator==(right));
    }
    bool operator<(const SimulatorResults& right) const
    {
        return (time < right.time);
    }
    std::ostream& operator<< (std::ostream& stream) const
    {
        stream << "[queue,thread,message] poolsize = [" << queuePool << "," << threadPool << "," << messageCount << "] "
               << " = " << time.total_milliseconds() << " ms" << "\n";
        return stream;
    }
};
/*
std::ostream& operator<< (std::ostream& stream, SimulatorResults& results)
{
    stream << "thread poolsize = " << results.threadPool
           << " messages = " << results.messageCount
           << " no of queues = " << results.queuePool
           << " time taken (ms) = " << results.time.total_milliseconds()
           << "\n";
    return stream;
}
*/
#endif // SIMULATOR_H
