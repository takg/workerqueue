#include "Simulator.hpp"
#include "PrintStream.hpp"
#include <set>

Simulator::Simulator()
{
}

Simulator::~Simulator()
{
    takg::cout << "Completed..." << "\n";
}

void Simulator::dumpResults()
{
    std::multiset<SimulatorResults> resultSet;

    // must start from 1
    for (int queuePoolSize = 1 ; queuePoolSize <= QUEUE_MAX_POOL ; ++queuePoolSize)
    {
        // must start from 1
        for (int threadPoolSize = 1 ; threadPoolSize <= THREAD_MAX_POOL ; ++threadPoolSize)
        {
            takg::TimeDifference diff;
            SimulatorResults results(threadPoolSize, queuePoolSize, MAX_MESSAGES);
            takg::cout << "Running for [queue,thread] pool size = [" << results.queuePool << "," << results.threadPool << "]";

            takg::QueueManager<std::string> queue(results.queuePool);
            PushDataSimulator pushData(results.messageCount, queue);
            boost::thread pushingThread(pushData);
            // let's just keep pushing data
            pushingThread.detach();

            takg::ThreadPool<> processingThreads(results.threadPool, true);
            PullDataSimulator pullData(results.threadPool, queue);
            // start thread for pushing messages in queue
            for (int threadCount = 0 ; threadCount < results.threadPool ; ++threadCount)
            {
                if (results.threadPool == results.queuePool)
                    pullData.setID(threadCount);
                else
                    pullData.setID(-1);

                processingThreads.start(pullData);
            }

            processingThreads.join();
            results.time = diff.getTime();
            takg::cout << " = " << results.time.total_milliseconds() << " ms." << "\n";
            resultSet.insert(results);
        }
    }

    takg::cout << "Best results are for:" << "\n";
    long prevTotal = -1;

    for (std::set<SimulatorResults>::const_iterator it = resultSet.begin(); it != resultSet.end() ; ++it)
    {
        if (prevTotal != -1 && prevTotal == it->time.total_milliseconds() || prevTotal == -1)
        {
            it->operator<<(std::cout);
        }
        else
        {
            break;
        }
        prevTotal = it->time.total_milliseconds();
    }

    return;
}

void Simulator::run()
{
    dumpResults();
    return;
}
