#ifndef QUEUEMANAGER_HPP_INCLUDED
#define QUEUEMANAGER_HPP_INCLUDED

#include <queue>
#include <boost/scoped_array.hpp>
#include "PrintStream.hpp"

namespace takg
{
    template <typename T,
              typename Container=std::queue<T>,
              typename Mutex=boost::mutex >
    class QueueManager
    {
    public:
        // constructor
        QueueManager(int queueCount):
            currentPushIndex(0),
            currentPopIndex(0),
            noOfQueues(queueCount)
        {
            queueList.reset(new Container[noOfQueues]);
            mutexes.reset(new Mutex[noOfQueues]);
        }

        // copy constructor
        QueueManager(const QueueManager& right):
            currentPushIndex(right.currentPushIndex),
            currentPopIndex(right.currentPopIndex),
            noOfQueues(right.noOfQueues)
        {
            queueList.reset(new Container[noOfQueues]);
            mutexes.reset(new Mutex[noOfQueues]);
        }

        // destructor
        ~QueueManager()
        {
        }

        // assignment operator
        QueueManager& operator=(const QueueManager& right)
        {
            if (&right != this)
            {
                currentPushIndex = right.currentPushIndex;
                currentPopIndex = right.currentPopIndex;
                noOfQueues = right.noOfQueues;
                queueList.reset(new Container[right.noOfQueues]);
                mutexes.reset(new Mutex[right.noOfQueues]);
            }
            return *this;
        }

        /*
        This function would be called to push in a round robin fashion to each of the queues
        in a sequential manner.
        */
        void push_back(const T& value)
        {
            if (currentPushIndex > noOfQueues)
            {
                takg::cout << "ERROR: Wrong Index(push_back)";
                throw std::exception();
            }
            boost::mutex::scoped_lock lock(mutexes[currentPushIndex]);
            queueList[currentPushIndex].push(value);
            currentPushIndex = (currentPushIndex+1) % noOfQueues;
            return;
        }

        /*
        This function is called when there is a particular queue being called for next message
        */
        bool pop_front(T& value, int index)
        {
            bool flag = false;
            if (index >= noOfQueues || index < 0)
            {
                takg::cout << "ERROR: Wrong Index (pop_front)" << index << "(" << noOfQueues << ")";
                throw std::exception();
            }
            else if ( queueList[index].size() != 0)
            {
                boost::mutex::scoped_lock lock(mutexes[index]);
                value = queueList[index].front();
                queueList[index].pop();
                flag = true;
            }
            return flag;
        }

        /*
        This function is called when, we are interested in processing the next message
        */
        bool pop_front(T& value)
        {
            bool flag = false;

            for (int nLoop = 0 ; nLoop < noOfQueues ; ++nLoop)
            {
                boost::mutex::scoped_lock lock(indexMutex);

                if (queueList[currentPopIndex].size() != 0)
                {
                    value = queueList[currentPopIndex].front();
                    queueList[currentPopIndex].pop();
                    flag = true;
                    break;
                }
                // when there are two different queues being read by diferrent threads
                // then change the index properly
                currentPopIndex = (currentPopIndex+1)%noOfQueues;
            }
            return flag;
        }
    private:
        int currentPushIndex;
        int currentPopIndex;
        int noOfQueues;
        boost::scoped_array<Container> queueList;
        boost::scoped_array<Mutex> mutexes;
        Mutex indexMutex;
    };
}

#endif // QUEUEMANAGER_HPP_INCLUDED
